# Hex Describe W

This software is a fork of [Alex Schroeder](https://alexschroeder.ch)'s
[Hex Describe](https://github.com/kensanata/hex-mapping).

Its purpose is to generate hexagonal maps for use with pen & paper roleplaying
games. **Hex Describe W** is a web application written in Perl 5 generating SVG
maps.

## Description

This application takes a text description of a map and a set of random tables to
generate a textual description of the region. It's ideal if your players are
wandering into unprepared regions, and it's great if you need some seed material
to base your work on.

## Use

Start the web server (in this case, on port 8080) with:
```
perl hex-describe.pl daemon -m production -l http://\*:8080
```

Then access **Hex Describe W** by visiting
[http://127.0.0.1:8080](http://127.0.0.1:8080) in a web browser.

## Format

The user can provide their own tables, the format is simple: every word in the
map description and every two word combo from the map description is a potential
table in the file.
If it exists, it will be used.

Assuming the following description:

```
0101 dark-green trees village
```

The description will be generated from any tables that match:

* dark-green
* trees
* village

It would make sense to just provide tables for "trees" and "village",
for example.

Tables looks like this:

```
;trees
1,some trees
1,you encounter [forest monster]

;forest monster
3,[3d6] bandits
1,an elf
```

A semicolon and some text begin a new table. A number, a comma, and
some text are an entry in he table. The text needs to be on one line.
The numbers are relative probabilities. The chances for an encounter
in the forest are thus 50% and the chances to encounter an elf, if you
are encountering anything at all, are 25%. The example also shows how
you can link from one table to another using square brackets.

Square brackets are also used for dice rolls. A dice roll can look
like this: 3d6, 3d6+5 3d6x10, or 3d6x10+5.

There's an built-in help page with more details.
