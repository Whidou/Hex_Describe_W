#!/usr/bin/env perl
# Copyright (C) 2018  Alex Schroeder <alex@gnu.org>
# Copyright (C) 2018  Whidou <root@whidou.fr>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

use Modern::Perl;
use Mojolicious::Lite;
use Mojo::UserAgent;
use Mojo::URL;
use Mojo::Log;
use Mojo::ByteStream;
use Array::Utils qw(intersect);
use utf8;
use feature 'unicode_strings';
use open ':encoding(UTF-8)';

plugin Config => {default => {loglevel => 'debug'}};

my $log = Mojo::Log->new;
$log->level(app->config('loglevel'));

my $default_map;
{
  local $/;
  open(FILE, 'default_map.hex') || die $!;
  $default_map = <FILE>;
  close(FILE);
}

my $default_table;
{
  local $/;
  open(FILE, 'default_table.abu') || die $!;
  $default_table = <FILE>;
  close(FILE);
}

# based on text-mapper.pl Mapper process
my $hex_re = qr/^(\d\d)(\d\d)(?:\s+([^"\r\n]+)?\s*(?:"(.+)"(?:\s+(\d+))?)?|$)/;
my $line_re = qr/^(\d\d\d\d(?:-\d\d\d\d)+)\s+(\S+)/;

# extra data for every hex: $extra->{"0101"}->{"type"} eq "river"
my $extra;
my %names;

sub init {
  %names = ();
  $extra = undef;
}

sub parse_map {
  my $map = shift;
  my $map_data;
  for my $hex (split(/\r?\n/, $map)) {
    if ($hex =~ /$hex_re/) {
      my ($x, $y, $types) = ($1, $2, $3);
      my @types = split(/ /, $types);
      $map_data->{"$x$y"} = \@types;
    }
  }
  my @lines;
  for my $hex (split(/\r?\n/, $map)) {
    if ($hex =~ /$line_re/) {
      my ($line, $type) = ($1, $2);
      my @points = compute_missing_points(split(/-/, $line));
      push(@lines, [$type, @points]);
    }
  }
  # longest rivers first
  @lines = sort { @$b <=> @$a } @lines;
  # for my $line (@lines) {
  #   $log->debug("@$line");
  # }
  for my $line (@lines) {
    my $type = shift(@$line);
    my %data = (type => $type);
    my $start = 1;
    for my $coord (@$line) {
      # $log->debug("$coord");
      # Don't add data for hexes outside the map (thus, rivers and trails must
      # come at the end).
      last unless $map_data->{$coord};
      # add river or trail to the hex description, add "$type-start" if the
      # first one, or add "$type-merge" when running into an existing one
      my $merged;
      if ($start) {
  if (grep { $_ eq $type } @{$map_data->{$coord}}) {
    # don't note a start in an existing river
    # $log->debug("Skipping $type start at $coord");
    last;
  } else {
    push(@{$map_data->{$coord}}, $type, "$type-start");
    $start = 0;
    # $log->debug("$type start at $coord");
  }
      } else {
  if (not grep { $_ eq $type } @{$map_data->{$coord}}) {
    push(@{$map_data->{$coord}}, $type);
    # $log->debug("...$type leading into $coord");
  } elsif (not grep { $_ eq "$type-merge" } @{$map_data->{$coord}}) {
    push(@{$map_data->{$coord}}, "$type-merge");
    $merged = 1;
    # $log->debug("...merging into $type at $coord");
  } else {
    $merged = 1;
    # $log->debug("...merged with other $type at $coord");
  }
      }
      # all river hexes share this hash and each hex can have a river, a
      # trail, a canyon, etc.
      push(@{$extra->{$coord}}, \%data);
      # if a river merges into another, don't add any hexes downriver
      last if $merged;
    }
  }
  return $map_data;
}

my $dice_re = qr/^(\d+)d(\d+)(?:[x*](\d+))?(?:\+(\d+))?(?:-(\d+))?$/;

sub parse_table {
  my $text = shift;
  $log->debug("parse_table: parsing " . length($text) . " characters");
  my $data = {};
  my $key;
  for my $line (split(/\r?\n/, $text)) {
    if ($line =~ /^;([^#\r\n]+)/) {
      $key = $1;
    } elsif ($key and $line =~ /^(\d+),(.+)/) {
      $data->{$key}->{total} += $1;
      my %h = (count => $1, text => $2);
      push(@{$data->{$key}->{lines}}, \%h);
    }
  }
  # check tables
  for my $table (keys %$data) {
    for my $line (@{$data->{$table}->{lines}}) {
      for my $subtable ($line->{text} =~ /\[([^\[\]\n]*?)\]/g) {
  next if $subtable =~ /$dice_re/;
  next if $subtable =~ /^redirect https?:/;
  next if $subtable =~ /names for (.*)/ and $data->{"name for $1"};
  $log->error("Error in table $table: subtable $subtable is missing")
      unless $data->{$subtable};
      }
    }
  }
  return $data;
}

sub pick_description {
  my $total = shift;
  my $lines = shift;
  my $roll = int(rand($total)) + 1;
  my $i = 0;
  for my $line (@$lines) {
    $i += $line->{count};
    if ($i >= $roll) {
      return $line->{text};
    }
  }
  return '';
}

sub resolve_redirect {
  # If you install this tool on a server using HTTPS, then some browsers will
  # make sure that including resources from other servers will not work.
  my $url = shift;
  my $ua = Mojo::UserAgent->new;
  my $res = $ua->get($url)->result;
  if ($res->code == 301 or $res->code == 302) {
    return Mojo::URL->new($res->headers->location)
  ->base(Mojo::URL->new($url))
  ->to_abs;
  }
  $log->info("resolving redirect for $url did not result in a redirection");
  return $url;
}

sub pick {
  my $map_data = shift;
  my $table_data = shift;
  my $level = shift;
  my $coordinates = shift;
  my $words = shift;
  my $word = shift;
  my $text;
  # Make sure we're testing all the context combinations first. Thus, if $words
  # is [ "mountains" white" "chaos"] and $word is "mountains", we want to test
  # "white mountains", "cold mountains" and "mountains", in this order.
  for my $context (grep( { $_ ne $word } @$words), $word) {
    my $key = ($context eq $word ? $word : "$context $word");
    # $log->debug("$coordinates: looking for a $key table") if $coordinates eq "0109";
    if ($table_data->{$key}) {
      my $total = $table_data->{$key}->{total};
      my $lines = $table_data->{$key}->{lines};
      $text = pick_description($total, $lines);
      # $log->debug("$coordinates: picked $text") if $coordinates eq "0109";
      $text =~ s/\[\[redirect (https:.*?)\]\]/
                 app->mode eq 'development' ? '' : resolve_redirect($1)/ge;
      $text =~ s/\[(.*?)\]/describe($map_data,$table_data,$level+1,$coordinates,[$1])/ge;
      last;
    }
  }
  return $text;
}

sub describe {
  my $map_data = shift;
  my $table_data = shift;
  my $level = shift;
  my $coordinates = shift;
  my $words = shift;
  return '' if $level > 10;
  my @descriptions;
  for my $word (@$words) {
    # valid dice rolls: 1d6, 1d6+1, 1d6x10, 1d6x10+1
    if (my ($n, $d, $m, $p, $s) = $word =~ /$dice_re/) {
      my $r = 0;
      for(my $i = 0; $i < $n; $i++) {
        $r += int(rand($d)) + 1;
      }
      $r *= $m||1;
      $r += $p||0;
      $r -= $s||0;
      # $log->debug("rolling dice: $word = $r");
      push(@descriptions, $r);
    } elsif ($word =~ /^name for a /) {
      # for global things like factions, dukes
      my $name = $names{$word};
      # $log->debug("Memoized: $word is $name") if $name;
      return $name if $name;
      $name = pick($map_data, $table_data, $level, $coordinates, $words, $word);
      next unless $name;
      $names{$word} = $name;
      # $log->debug("$word is $name");
      push(@descriptions, $name);
    } elsif ($word =~ /^names for (\S+)/) {
      my $key = $1; # "river"
      # $log->debug("Looking at $key for $coordinates...");
      if (my @lines = grep { $_->{type} eq $key } @{$extra->{$coordinates}}) {
        # $log->debug("...@lines");
        # make sure all the lines (rivers, trails) are named
        my @names = ();
        for my $line (@lines) {
          my $name = $line->{name};
          if (not $name) {
            $name ||= pick($map_data, $table_data, $level, $coordinates, $words,
                           "name for $key");
            $line->{name} = $name;
          }
          push(@names, $name);
        }
        my $list;
        if (@names > 2) {
          $list = join(", ", @names[0 .. $#names-1], "and " . $names[-1]);
        } elsif (@names == 2) {
          $list = join(" and ", @names);
        } else {
          $log->error("$coordinates has merge but just one line (@lines)");
          $list = shift(@names);
        }
        unless ($list) {
          $log->error("$coordinates uses merging rule without names");
          next;
        }
        push(@descriptions, $list);
      }
    } elsif ($word =~ /^name for (\S+)/) {
      my $key = $1; # "white" or "river"
      # $log->debug("Looking at $key for $coordinates...");
      if (my @lines = grep { $_->{type} eq $key } @{$extra->{$coordinates}}) {
        # for rivers and the like: "name for river"
        for my $line (@lines) {
          # $log->debug("Looking at $word for $coordinates...");
          my $name = $line->{name};
          # $log->debug("... we already have a name: $name") if $name;
          # if a type appears twice for a hex, this returns the same name for
          # all of them
          return $name if $name;
          $name = pick($map_data, $table_data, $level, $coordinates, $words,
                       $word);
          # $log->debug("... we picked a new name: $name") if $name;
          next unless $name;
          push(@descriptions, $name);
          $line->{name} = $name;
          # name the first one without a name, don't keep adding names
          last;
        }
      } else {
        # regular featuers: "name for white big mountain"
        my $name = $names{"$word: $coordinates"}; # "name for white big mountain: 0101"
        return $name if $name;
        $name = pick($map_data, $table_data, $level, $coordinates, $words, $word);
        next unless $name;
        push(@descriptions, $name);
        spread_name($map_data, $coordinates, $word, $key, $name);
      }
    } else {
      my $text = pick($map_data, $table_data, $level, $coordinates, $words, $word);
      next unless $text;
      push(@descriptions, '<div class="hex_description">') if $level == 1;
      push(@descriptions, $text);
      push(@descriptions, '</div>') if $level == 1;
    }
  }

  foreach (@descriptions)
  {
    # Fix use of a/an in English
    s/(^|[[:space:]])(a)n? \(a(n?)\) /$1$2$3 /gi;
    s/ \(an?\) / /g;

    # Put bold font weight for emphasis where needed
    s/\*(.*?)\*/<strong>$1<\/strong>/g;
  }

  return join(' ', @descriptions);
}

sub process {
  my $text = shift;
  my @terms = split(/(<img.*?>)/, $text);
  return { images => '', html => $text } unless @terms > 1;
  my @output; # $output[0] is texts, $output[1] is images
  my $i = 0;
  while (@terms) {
    push(@{$output[$i]}, shift(@terms));
    $i = 1 - $i;
  }
  return {
    images => '<span class="images">' . join('', @{$output[1]}) . '</span>',
    html => join('', @{$output[0]}),
  };
}

sub describe_map {
  my $map_data = shift;
  my $table_data = shift;
  my %descriptions;
  for my $coord (keys %$map_data) {
    $descriptions{$coord} = process(describe($map_data, $table_data, 1,
                                    $coord, $map_data->{$coord}));
  }
  return \%descriptions;
}

# x is even on first line, x is odd on second
my $delta = [[[-1,  0], [ 0, -1], [+1,  0], [+1, +1], [ 0, +1], [-1, +1]],
             [[-1, -1], [ 0, -1], [+1, -1], [+1,  0], [ 0, +1], [-1,  0]]];

sub xy {
  my $coordinates = shift;
  return (substr($coordinates, 0, 2), substr($coordinates, 2));
}

sub coordinates {
  my ($x, $y) = @_;
  return sprintf("%02d%02d", $x, $y);
}

sub neighbour {
  # $hex is [x,y] or "0101" and $i is a number 0 .. 5
  my ($hex, $i) = @_;
  $hex = [xy($hex)] unless ref $hex;
  # return is a string like "0102"
  return coordinates(
    $hex->[0] + $delta->[$hex->[0] % 2]->[$i]->[0],
    $hex->[1] + $delta->[$hex->[0] % 2]->[$i]->[1]);
}

sub one_step_to {
  my $from = shift;
  my $to = shift;
  my ($min, $best);
  for my $i (0 .. 5) {
    # make a new guess
    my ($x, $y) = ($from->[0] + $delta->[$from->[0] % 2]->[$i]->[0],
                   $from->[1] + $delta->[$from->[0] % 2]->[$i]->[1]);
    my $d = ($to->[0] - $x) * ($to->[0] - $x) +
            ($to->[1] - $y) * ($to->[1] - $y);
    if (!defined($min) || $d < $min) {
      $min = $d;
      $best = [$x, $y];
    }
  }
  return $best;
}

sub compute_missing_points {
  my @result = ($_[0]); # "0101" not [01,02]
  my @points = map { [xy($_)] } @_;
  # $log->debug("Line: " . join(", ", map { coordinates(@$_) } @points));
  my $from = shift(@points);
  while (@points) {
    # $log->debug("Going from " . coordinates(@$from) . " to " . coordinates(@{$points[0]}));
    $from = one_step_to($from, $points[0]);
    shift(@points) if $from->[0] == $points[0]->[0] and $from->[1] == $points[0]->[1];
    push(@result, coordinates(@$from));
  }
  return @result;
}

sub spread_name {
  my $map_data = shift;
  my $coordinates = shift;
  my $word = shift; # "name for white big mountain"
  my $key = shift; # "white"
  my @keys = split(/\//, $key); # ("white")
  my $name = shift; # "Vesuv"
  my %seen = ($coordinates => 1);
  # $log->debug("$word: $coordinates = $name");
  my @queue = map { neighbour($coordinates, $_) } 0..5;
  while (@queue) {
    # $log->debug("Working on the first item of @queue");
    my $coord = shift(@queue);
    next if $seen{$coord} or not $map_data->{$coord};
    $seen{$coord} = 1;
    if (intersect(@keys, @{$map_data->{$coord}})) {
      if ($names{"$word for $coord"}) {
        $log->error("$word for $coord is already something else");
      }
      $names{"$word: $coord"} = $name; # "name for white big mountain: 0102"
      # $log->debug("$word: $coord = $name");
      push(@queue, map { neighbour($coord, $_) } 0..5);
    }
  }
}

sub describe_text {
  my $input = shift;
  my $table_data = shift;
  my @descriptions;
  for my $text (split(/\r?\n/, $input)) {
    # $log->debug("replacing lookups in $text");
    $text =~ s/\[(.*?)\]/describe({},$table_data,1,"",[$1])/ge;
    push(@descriptions, process($text));
  }
  return \@descriptions;
}

helper example => sub {
  my ($c, $block) = @_;
  my $result = $block->();
  my $url;
  if ($result =~ /^\d\d\d\d/m) {
    my $map = join("\n", grep(/^\d\d\d\d|^theme/, split(/\n/, $result)));
    my $table = join("\n", grep(!/^\d\d\d\d|^theme/, split(/\n/, $result)));
    $url = $c->url_for('edit')->query(map => $map, table=> $table);
  } else {
    my ($key) = $result =~ /^;(.*)/m;
    $url = $c->url_for('nomap')->query(input => "[$key]\n" x 4,
                                       table=> $result);
  }
  return Mojo::ByteStream->new(qq(<pre>$result</pre><p><a href='$url'>Try it</a>.</p>));
};

get '/' => sub {
  my $c = shift;
  my $map = $c->param('map') || $default_map;
  my $url = $c->param('url');
  my $table = $c->param('table');
  $c->render(template => 'edit', map => $map, url => $url, table => $table);
} => 'edit';

get '/load/random/smale' => sub {
  my $c = shift;
  my $map = `perl text-mapper.pl random`;
  $c->render(template => 'edit', map => $map, url=>'', table => '');
};

get '/load/random/alpine' => sub {
  my $c = shift;
  my $map = `perl text-mapper.pl alpine`;
  $c->render(template => 'edit', map => $map, url=>'', table => '');
};

any '/describe' => sub {
  my $c = shift;
  my $map = $c->param('map');
  my $svg = `perl text-mapper.pl render <<__EOF__\n$map\n__EOF__`;
  my $url = $c->param('url');
  my $table;
  $table = $c->param('table');
  $table ||= $default_table;
  init();
  $c->render(template => 'description',
             svg => $svg,
             descriptions => describe_map(parse_map($map),
                                          parse_table($table)));
};

get '/nomap' => sub {
  my $c = shift;
  my $input = $c->param('input') || '';
  my $url = $c->param('url');
  my $table = $c->param('table');
  $c->render(template => 'nomap', input => $input, url => $url, table => $table);
};

any '/describe/text' => sub {
  my $c = shift;
  my $input = $c->param('input');
  my $url = $c->param('url');
  my $table;
  $table = $c->param('table');
  $table ||= $default_table;
  init();
  $c->render(template => 'text',
             descriptions => describe_text($input, parse_table($table)));
};

get '/default/map' => sub {
  my $c = shift;
  $c->render(text => $default_map, format => 'txt');
};

get '/default/table' => sub {
  my $c = shift;
  $c->render(text => $default_table, format => 'txt');
};

get '/source' => sub {
  my $c = shift;
  seek(DATA,0,0);
  local $/ = undef;
  $c->render(text => <DATA>, format => 'txt');
};

get '/authors' => sub {
  my $c = shift;
  $c->render(template => 'authors');
};

get '/help' => sub {
  my $c = shift;
  $c->render(template => 'help');
};

app->start;
